# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Maciej <maciekw5@gmail.com>, 2010.
# Marta Rybczyńska <kde-i18n@rybczynska.net>, 2010.
# Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>, 2011, 2012, 2014, 2015, 2018, 2019, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-16 00:50+0000\n"
"PO-Revision-Date: 2022-04-30 21:21+0200\n"
"Last-Translator: Łukasz Wojniłowicz <lukasz.wojnilowicz@gmail.com>\n"
"Language-Team: Polish <kde-i18n-doc@kde.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 22.03.70\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Łukasz Wojniłowicz"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "lukasz.wojnilowicz@gmail.com"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Dane wyjściowe"

#. i18n: ectx: property (text), widget (QLabel, showLabel)
#: build.ui:56
#, kde-format
msgid "Show:"
msgstr "Pokaż:"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton2)
#: build.ui:121 build.ui:146
#, kde-format
msgid "Build again"
msgstr "Zbuduj ponownie"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton2)
#: build.ui:128 build.ui:153
#, kde-format
msgid "Cancel"
msgstr "Anuluj"

#. i18n: ectx: property (text), widget (QTreeWidget, errTreeWidget)
#: build.ui:184
#, kde-format
msgctxt "Header for the file name column"
msgid "File"
msgstr "Plik"

#. i18n: ectx: property (text), widget (QTreeWidget, errTreeWidget)
#: build.ui:189
#, kde-format
msgctxt "Header for the line number column"
msgid "Line"
msgstr "Linia"

#. i18n: ectx: property (text), widget (QTreeWidget, errTreeWidget)
#: build.ui:194
#, kde-format
msgctxt "Header for the error message column"
msgid "Message"
msgstr "Wiadomość"

#: plugin_katebuild.cpp:136 plugin_katebuild.cpp:143 plugin_katebuild.cpp:1325
#, kde-format
msgid "Build"
msgstr "Buduj"

#: plugin_katebuild.cpp:146
#, kde-format
msgid "Select Target..."
msgstr "Wybierz cel..."

#: plugin_katebuild.cpp:151
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build Selected Target"
msgstr "Zbuduj wybrany cel"

#: plugin_katebuild.cpp:156
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build and Run Selected Target"
msgstr "Zbuduj wybrany cel"

#: plugin_katebuild.cpp:161
#, kde-format
msgid "Stop"
msgstr "Zatrzymaj"

#: plugin_katebuild.cpp:166
#, kde-format
msgid "Next Error"
msgstr "Następny błąd"

#: plugin_katebuild.cpp:172
#, kde-format
msgid "Previous Error"
msgstr "Poprzedni błąd"

#: plugin_katebuild.cpp:178
#, kde-format
msgid "Show Marks"
msgstr "Pokaż znaczniki"

#: plugin_katebuild.cpp:187
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Ustawienia celu"

#: plugin_katebuild.cpp:347 plugin_katebuild.cpp:1427 plugin_katebuild.cpp:1438
#: plugin_katebuild.cpp:1459 plugin_katebuild.cpp:1469
#, kde-format
msgid "Project Plugin Targets"
msgstr "Cele wtyczki projektu"

#: plugin_katebuild.cpp:500
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"the working directory in the Target Settings"
msgstr ""
"<title>Nie można otworzyć pliku:</title><nl/>%1<br/>Spróbuj dodać ścieżkę "
"wyszukiwania do katalogu pracy w ustawieniach celów"

#: plugin_katebuild.cpp:520
#, kde-format
msgctxt "The same word as 'make' uses to mark an error."
msgid "error"
msgstr "błąd"

#: plugin_katebuild.cpp:523
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "nieokreślone odniesienie"

#: plugin_katebuild.cpp:530
#, kde-format
msgctxt "The same word as 'make' uses to mark a warning."
msgid "warning"
msgstr "ostrzeżenie"

#: plugin_katebuild.cpp:616
#, kde-format
msgid "Error"
msgstr "Błąd"

#: plugin_katebuild.cpp:621
#, kde-format
msgid "Warning"
msgstr "Ostrzeżenie"

#: plugin_katebuild.cpp:752
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "Nie wybrano pliku lub katalogu dla kompilacji."

#: plugin_katebuild.cpp:756
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""
"Plik \"%1\" nie jest plikiem lokalnym. Kompilować można tylko pliki lokalne."

#: plugin_katebuild.cpp:803
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""
"Nie można wykonać polecenia: %1\n"
"Nie istnieje ścieżka pracy: %2"

#: plugin_katebuild.cpp:822
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "Nie udało się uruchomić \"%1\". Wynik = %2"

#: plugin_katebuild.cpp:841
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "Porzucono budowanie <b>%1</b>"

#: plugin_katebuild.cpp:942
#, kde-format
msgid "No target available for building."
msgstr "Brak celu do budowania."

#: plugin_katebuild.cpp:956
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "Nie wybrano lokalnego pliku lub katalogu dla budowania."

#: plugin_katebuild.cpp:962
#, kde-format
msgid "Already building..."
msgstr "Już w trakcie budowy..."

#: plugin_katebuild.cpp:989
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "Budowanie celu <b>%1</b> ..."

#: plugin_katebuild.cpp:1004
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>Wyniki Make:</title><nl/>%1"

#: plugin_katebuild.cpp:1041
#, kde-format
msgid "Building <b>%1</b> completed."
msgstr "Ukończono budowanie <b>%1</b>."

#: plugin_katebuild.cpp:1060
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Znaleziono jeden błąd."
msgstr[1] "Znaleziono %1 błędy."
msgstr[2] "Znaleziono %1 błędów."

#: plugin_katebuild.cpp:1061
#, kde-format
msgid "Building <b>%1</b> had errors."
msgstr "Budowanie <b>%1</b> zawiera błędy."

#: plugin_katebuild.cpp:1063
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Znaleziono jedno ostrzeżenie."
msgstr[1] "Znaleziono %1 ostrzeżenia."
msgstr[2] "Znaleziono %1 ostrzeżeń."

#: plugin_katebuild.cpp:1064
#, kde-format
msgid "Building <b>%1</b> had warnings."
msgstr "Budowanie <b>%1</b> zawiera ostrzeżenia."

#: plugin_katebuild.cpp:1070
#, kde-format
msgid "Build failed."
msgstr "Budowanie nieudane."

#: plugin_katebuild.cpp:1072
#, kde-format
msgid "Build completed without problems."
msgstr "Budowanie zakończone bez problemów."

#: plugin_katebuild.cpp:1103
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr ""

#: plugin_katebuild.cpp:1324
#, kde-format
msgid "Target Set"
msgstr "Zestaw celów"

#: plugin_katebuild.cpp:1326
#, kde-format
msgid "Clean"
msgstr "Oczyść"

#: plugin_katebuild.cpp:1327
#, kde-format
msgid "Config"
msgstr "Ustawienia"

#: plugin_katebuild.cpp:1328
#, kde-format
msgid "ConfigClean"
msgstr "Oczyszczenie ustawień"

#: plugin_katebuild.cpp:1371
#, kde-format
msgid "Only Errors"
msgstr "Tylko błędy"

#: plugin_katebuild.cpp:1374
#, kde-format
msgid "Errors and Warnings"
msgstr "Błędy i ostrzeżenia"

#: plugin_katebuild.cpp:1377
#, kde-format
msgid "Parsed Output"
msgstr "Przetworzony wynik"

#: plugin_katebuild.cpp:1380
#, kde-format
msgid "Full Output"
msgstr "Pełny wynik"

#: plugin_katebuild.cpp:1497
#, kde-format
msgid "build"
msgstr "buduj"

#: plugin_katebuild.cpp:1500
#, kde-format
msgid "clean"
msgstr "oczyść"

#: plugin_katebuild.cpp:1503
#, kde-format
msgid "quick"
msgstr "szybko"

#: TargetHtmlDelegate.cpp:47
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>T:</B> %1"

#: TargetHtmlDelegate.cpp:49
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>Dir:</B> %1"

#: TargetHtmlDelegate.cpp:98
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"Pozostaw puste, aby użyć katalogu bieżącego dokumentu.\n"
"Aby dodać katalog wyszukiwania, dodawaj ścieżki oddzielone znakiem ';'"

#: TargetHtmlDelegate.cpp:102
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Użycie:\n"
"\"%f\" dla bieżącego pliku\n"
"\"%d\" dla katalogu bieżącego pliku\n"
"\"%n\" dla bieżącej nazwy pliku bez przyrostka"

#: TargetModel.cpp:339
#, kde-format
msgid "Check the check-box to make the command the default for the target-set."
msgstr "Zaznacz pole, aby uczynić to polecenie domyślnym dla zestawu celów."

#: TargetModel.cpp:395
#, kde-format
msgid "Command/Target-set Name"
msgstr "Nazwa polecenia/zestawu celów"

#: TargetModel.cpp:398
#, kde-format
msgid "Working Directory / Command"
msgstr "Katalog roboczy / polecenie"

#: TargetModel.cpp:401
#, fuzzy, kde-format
#| msgid "Command:"
msgid "Run Command"
msgstr "Polecenie:"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "Utwórz nowy zestaw celów"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "Skopiuj polecenie lub zestaw celów"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr "Usuń bieżący cel lub zestaw celów"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "Dodaj nowy cel"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "Zbuduj wybrany cel"

#: targets.cpp:48
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build and run selected target"
msgstr "Zbuduj wybrany cel"

#: targets.cpp:52
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Move selected target up"
msgstr "Zbuduj wybrany cel"

#: targets.cpp:56
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Move selected target down"
msgstr "Zbuduj wybrany cel"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "&Buduj"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "Wstaw ścieżkę"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "Wybierz katalog do wstawienia"

#~ msgid "Select active target set"
#~ msgstr "Wybierz aktywny zestaw celów"

#~ msgid "Filter targets"
#~ msgstr "Odfiltruj cele"

#~ msgid "Build Default Target"
#~ msgstr "Zbuduj domyślny cel"

#, fuzzy
#~| msgid "Build Default Target"
#~ msgid "Build and Run Default Target"
#~ msgstr "Zbuduj domyślny cel"

#~ msgid "Build Previous Target"
#~ msgstr "Zbuduj poprzedni cel"

#~ msgid "Active target-set:"
#~ msgstr "Aktywny zestaw celów:"

#~ msgid "config"
#~ msgstr "ustawienia"

#~ msgid "Kate Build Plugin"
#~ msgstr "Wtyczka budowania Kate"

#~ msgid "Select build target"
#~ msgstr "Wybierz cel budowania"

#~ msgid "Filter"
#~ msgstr "Filtr"

#~ msgid "Build Output"
#~ msgstr "Wynik budowania"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "<title>Make Results:</title><nl/>%1"
#~ msgctxt "@info"
#~ msgid "<title>Could not open file:</title><nl/>%1"
#~ msgstr "<title>Wyniki Make:</title><nl/>%1"

#~ msgid "Next Set of Targets"
#~ msgstr "Następny zestaw celów"

#~ msgid "No previous target to build."
#~ msgstr "Brak poprzednich celów do zbudowania."

#~ msgid "No target set as default target."
#~ msgstr "Brak zestawu celów ustawionych jako domyślny cel."

#~ msgid "No target set as clean target."
#~ msgstr "Brak zestawu celów ustawionych jako czysty cel."

#~ msgid "Target \"%1\" not found for building."
#~ msgstr "Nie znaleziono celu \"%1\" do budowania."

#~ msgid "Really delete target %1?"
#~ msgstr "Czy na pewno usunąć cel %1?"

#~ msgid "Nothing built yet."
#~ msgstr "Jeszcze nic nie zbudowano."

#~ msgid "Target Set %1"
#~ msgstr "Zestaw celów %1"

#~ msgid "Target"
#~ msgstr "Cel"

#~ msgid "Target:"
#~ msgstr "Cel:"

#~ msgid "from"
#~ msgstr "od"

#~ msgid "Sets of Targets"
#~ msgstr "Zestaw celów"

#~ msgid "Make Results"
#~ msgstr "Wyniki polecenia kompilacji"

#~ msgid "Others"
#~ msgstr "Inne"

#~ msgid "Quick Compile"
#~ msgstr "Szybka kompilacja"

#~ msgid "The custom command is empty."
#~ msgstr "Pole własnego polecenia jest puste."

#~ msgid "New"
#~ msgstr "Nowy"

#~ msgid "Copy"
#~ msgstr "Kopiuj"

#~ msgid "Delete"
#~ msgstr "Usuń"

#~ msgid "Quick compile"
#~ msgstr "Szybka kompilacja"

#~ msgid "Run make"
#~ msgstr "Uruchom make"

#~ msgid "Build command:"
#~ msgstr "Polecenie kompilacji:"

#~ msgid "Break"
#~ msgstr "Przerwij"
