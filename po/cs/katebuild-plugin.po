# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Vít Pelčák <vit@pelcak.org>, 2010, 2011, 2012, 2013, 2014, 2017, 2019, 2020.
# Tomáš Chvátal <tomas.chvatal@gmail.com>, 2012.
# Vit Pelcak <vpelcak@suse.cz>, 2021.
#
msgid ""
msgstr ""
"Project-Id-Version: katebuild-plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-16 00:50+0000\n"
"PO-Revision-Date: 2021-08-19 11:55+0200\n"
"Last-Translator: Vit Pelcak <vpelcak@suse.cz>\n"
"Language-Team: Czech <kde-i18n-doc@kde.org>\n"
"Language: cs\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 21.08.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Vít Pelčák"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "vit@pelcak.org"

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "Výstup"

#. i18n: ectx: property (text), widget (QLabel, showLabel)
#: build.ui:56
#, kde-format
msgid "Show:"
msgstr "Zobrazit:"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton2)
#: build.ui:121 build.ui:146
#, kde-format
msgid "Build again"
msgstr "Znovu sestavit"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton2)
#: build.ui:128 build.ui:153
#, kde-format
msgid "Cancel"
msgstr "Zrušit"

#. i18n: ectx: property (text), widget (QTreeWidget, errTreeWidget)
#: build.ui:184
#, kde-format
msgctxt "Header for the file name column"
msgid "File"
msgstr "Soubor"

#. i18n: ectx: property (text), widget (QTreeWidget, errTreeWidget)
#: build.ui:189
#, kde-format
msgctxt "Header for the line number column"
msgid "Line"
msgstr "Řádek"

#. i18n: ectx: property (text), widget (QTreeWidget, errTreeWidget)
#: build.ui:194
#, kde-format
msgctxt "Header for the error message column"
msgid "Message"
msgstr "Zpráva"

#: plugin_katebuild.cpp:136 plugin_katebuild.cpp:143 plugin_katebuild.cpp:1325
#, kde-format
msgid "Build"
msgstr "Překlad"

#: plugin_katebuild.cpp:146
#, kde-format
msgid "Select Target..."
msgstr "Vyberte cíl..."

#: plugin_katebuild.cpp:151
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build Selected Target"
msgstr "Sestavit zvolený cíl"

#: plugin_katebuild.cpp:156
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build and Run Selected Target"
msgstr "Sestavit zvolený cíl"

#: plugin_katebuild.cpp:161
#, kde-format
msgid "Stop"
msgstr "Zastavit"

#: plugin_katebuild.cpp:166
#, kde-format
msgid "Next Error"
msgstr "Následující chyba"

#: plugin_katebuild.cpp:172
#, kde-format
msgid "Previous Error"
msgstr "Předchozí chyba"

#: plugin_katebuild.cpp:178
#, kde-format
msgid "Show Marks"
msgstr "Zobrazit značky"

#: plugin_katebuild.cpp:187
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "Nastavení cíle"

#: plugin_katebuild.cpp:347 plugin_katebuild.cpp:1427 plugin_katebuild.cpp:1438
#: plugin_katebuild.cpp:1459 plugin_katebuild.cpp:1469
#, kde-format
msgid "Project Plugin Targets"
msgstr ""

#: plugin_katebuild.cpp:500
#, kde-kuit-format
msgctxt "@info"
msgid ""
"<title>Could not open file:</title><nl/>%1<br/>Try adding a search path to "
"the working directory in the Target Settings"
msgstr ""

#: plugin_katebuild.cpp:520
#, kde-format
msgctxt "The same word as 'make' uses to mark an error."
msgid "error"
msgstr "chyba"

#: plugin_katebuild.cpp:523
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "nedefinovaný odkaz"

#: plugin_katebuild.cpp:530
#, kde-format
msgctxt "The same word as 'make' uses to mark a warning."
msgid "warning"
msgstr "varování"

#: plugin_katebuild.cpp:616
#, kde-format
msgid "Error"
msgstr "Chyba"

#: plugin_katebuild.cpp:621
#, kde-format
msgid "Warning"
msgstr "Varování"

#: plugin_katebuild.cpp:752
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "Pro sestavení není určen soubor nebo adresář."

#: plugin_katebuild.cpp:756
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr ""
"Soubor \"%1\" není místním souborem. Lze kopírovat pouze místní soubory."

#: plugin_katebuild.cpp:803
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""

#: plugin_katebuild.cpp:822
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "Nepovedlo se spustit \"%1\". exitStatus = %2"

#: plugin_katebuild.cpp:841
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr ""

#: plugin_katebuild.cpp:942
#, kde-format
msgid "No target available for building."
msgstr ""

#: plugin_katebuild.cpp:956
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "Pro sestavení není určen místní soubor nebo adresář."

#: plugin_katebuild.cpp:962
#, kde-format
msgid "Already building..."
msgstr ""

#: plugin_katebuild.cpp:989
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "Sestavuji cíl <b>%1</b> ..."

#: plugin_katebuild.cpp:1004
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr ""

#: plugin_katebuild.cpp:1041
#, kde-format
msgid "Building <b>%1</b> completed."
msgstr ""

#: plugin_katebuild.cpp:1060
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "Nalezena jedna chyba."
msgstr[1] "Nalezena %1 chyby."
msgstr[2] "Nalezeno %1 chyb."

#: plugin_katebuild.cpp:1061
#, kde-format
msgid "Building <b>%1</b> had errors."
msgstr ""

#: plugin_katebuild.cpp:1063
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "Nalezeno jedno varování."
msgstr[1] "Nalezena %1 varování."
msgstr[2] "Nalezeno %1 varování."

#: plugin_katebuild.cpp:1064
#, kde-format
msgid "Building <b>%1</b> had warnings."
msgstr "Při sestavování <b>%1</b> se vyskytla varování."

#: plugin_katebuild.cpp:1070
#, kde-format
msgid "Build failed."
msgstr "Sestavení selhalo."

#: plugin_katebuild.cpp:1072
#, kde-format
msgid "Build completed without problems."
msgstr "Sestavení dokončeno bez problémů."

#: plugin_katebuild.cpp:1103
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr ""

#: plugin_katebuild.cpp:1324
#, kde-format
msgid "Target Set"
msgstr "Sada cílů"

#: plugin_katebuild.cpp:1326
#, kde-format
msgid "Clean"
msgstr "Vyčistit"

#: plugin_katebuild.cpp:1327
#, kde-format
msgid "Config"
msgstr "Nastavení"

#: plugin_katebuild.cpp:1328
#, kde-format
msgid "ConfigClean"
msgstr ""

#: plugin_katebuild.cpp:1371
#, kde-format
msgid "Only Errors"
msgstr "Pouze chyby"

#: plugin_katebuild.cpp:1374
#, kde-format
msgid "Errors and Warnings"
msgstr "Chyby a varování"

#: plugin_katebuild.cpp:1377
#, kde-format
msgid "Parsed Output"
msgstr "Zpracovaný výstup"

#: plugin_katebuild.cpp:1380
#, kde-format
msgid "Full Output"
msgstr "Plný výstup"

#: plugin_katebuild.cpp:1497
#, kde-format
msgid "build"
msgstr "překlad"

#: plugin_katebuild.cpp:1500
#, kde-format
msgid "clean"
msgstr "vymazat"

#: plugin_katebuild.cpp:1503
#, kde-format
msgid "quick"
msgstr ""

#: TargetHtmlDelegate.cpp:47
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr ""

#: TargetHtmlDelegate.cpp:49
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>Adr:</B> %1"

#: TargetHtmlDelegate.cpp:98
#, kde-format
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr ""
"Ponechte prázdné pro použití adresáře současného dokumentu.\n"
"Přidejte adresáře pro hledání přidání cest oddělených ';'"

#: TargetHtmlDelegate.cpp:102
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"Použití:\n"
"\"%f\" pro aktuální soubor\n"
"\"%d\" pro adresář aktuálního souboru\n"
"\"%n\" pro název aktuálního souboru bez přípony"

#: TargetModel.cpp:339
#, kde-format
msgid "Check the check-box to make the command the default for the target-set."
msgstr ""

#: TargetModel.cpp:395
#, kde-format
msgid "Command/Target-set Name"
msgstr ""

#: TargetModel.cpp:398
#, kde-format
msgid "Working Directory / Command"
msgstr "Pracovní adresář / Příkaz"

#: TargetModel.cpp:401
#, kde-format
msgid "Run Command"
msgstr "Spustit příkaz"

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "Vytvořit novou sadu cílů"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "Kopírovat příkaz nebo sadu cílů"

#: targets.cpp:35
#, kde-format
msgid "Delete current target or current set of targets"
msgstr ""

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "Přidat nový cíl"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "Sestavit zvolený cíl"

#: targets.cpp:48
#, kde-format
msgid "Build and run selected target"
msgstr ""

#: targets.cpp:52
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Move selected target up"
msgstr "Sestavit zvolený cíl"

#: targets.cpp:56
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Move selected target down"
msgstr "Sestavit zvolený cíl"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "Pře&klad"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "Vložit cestu"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "Vyberte adresář pro vložení"

#~ msgid "Select active target set"
#~ msgstr "Vyberte aktivní sadu cílů"

#~ msgid "Build Default Target"
#~ msgstr "Sestavit výchozí cíl"

#~ msgid "Build Previous Target"
#~ msgstr "Sestavit předchozí cíl"
