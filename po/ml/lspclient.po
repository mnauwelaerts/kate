# Malayalam translations for kate package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the kate package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-13 00:49+0000\n"
"PO-Revision-Date: 2019-08-11 02:23+0200\n"
"Last-Translator: Automatically generated\n"
"Language-Team: Swathanthra|സ്വതന്ത്ര Malayalam|മലയാളം Computing|കമ്പ്യൂട്ടിങ്ങ് <smc."
"org.in>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:250
#, kde-format
msgid "Filter..."
msgstr ""

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:101 lspclientconfigpage.cpp:106
#: lspclientpluginview.cpp:596 lspclientpluginview.cpp:733 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr ""

#: lspclientconfigpage.cpp:224
#, kde-format
msgid "No JSON data to validate."
msgstr ""

#: lspclientconfigpage.cpp:233
#, kde-format
msgid "JSON data is valid."
msgstr ""

#: lspclientconfigpage.cpp:235
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr ""

#: lspclientconfigpage.cpp:238
#, kde-format
msgid "JSON data is invalid: %1"
msgstr ""

#: lspclientconfigpage.cpp:286
#, kde-format
msgid "Delete selected entries"
msgstr ""

#: lspclientconfigpage.cpp:291
#, kde-format
msgid "Delete all entries"
msgstr ""

#: lspclientplugin.cpp:228
#, kde-format
msgid "LSP server start requested"
msgstr ""

#: lspclientplugin.cpp:231
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""

#: lspclientplugin.cpp:244
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""

#: lspclientpluginview.cpp:608
#, kde-format
msgid "Go to Definition"
msgstr ""

#: lspclientpluginview.cpp:610
#, kde-format
msgid "Go to Declaration"
msgstr ""

#: lspclientpluginview.cpp:612
#, kde-format
msgid "Go to Type Definition"
msgstr ""

#: lspclientpluginview.cpp:614
#, kde-format
msgid "Find References"
msgstr ""

#: lspclientpluginview.cpp:617
#, kde-format
msgid "Find Implementations"
msgstr ""

#: lspclientpluginview.cpp:619
#, kde-format
msgid "Highlight"
msgstr ""

#: lspclientpluginview.cpp:621
#, kde-format
msgid "Symbol Info"
msgstr ""

#: lspclientpluginview.cpp:623
#, kde-format
msgid "Search and Go to Symbol"
msgstr ""

#: lspclientpluginview.cpp:628
#, kde-format
msgid "Format"
msgstr ""

#: lspclientpluginview.cpp:631
#, kde-format
msgid "Rename"
msgstr ""

#: lspclientpluginview.cpp:635
#, kde-format
msgid "Expand Selection"
msgstr ""

#: lspclientpluginview.cpp:638
#, kde-format
msgid "Shrink Selection"
msgstr ""

#: lspclientpluginview.cpp:642
#, kde-format
msgid "Switch Source Header"
msgstr ""

#: lspclientpluginview.cpp:645
#, kde-format
msgid "Expand Macro"
msgstr ""

#: lspclientpluginview.cpp:647
#, kde-format
msgid "Quick Fix"
msgstr ""

#: lspclientpluginview.cpp:650
#, kde-format
msgid "Code Action"
msgstr ""

#: lspclientpluginview.cpp:656
#, kde-format
msgid "Show selected completion documentation"
msgstr ""

#: lspclientpluginview.cpp:659
#, kde-format
msgid "Enable signature help with auto completion"
msgstr ""

#: lspclientpluginview.cpp:662
#, kde-format
msgid "Include declaration in references"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:665 lspconfigwidget.ui:68
#, kde-format
msgid "Add parentheses upon function completion"
msgstr ""

#: lspclientpluginview.cpp:668
#, kde-format
msgid "Show hover information"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:671 lspconfigwidget.ui:234
#, kde-format
msgid "Format on typing"
msgstr ""

#: lspclientpluginview.cpp:674
#, kde-format
msgid "Incremental document synchronization"
msgstr ""

#: lspclientpluginview.cpp:677
#, kde-format
msgid "Highlight goto location"
msgstr ""

#: lspclientpluginview.cpp:682
#, kde-format
msgid "Show Diagnostics Notifications"
msgstr ""

#: lspclientpluginview.cpp:685
#, kde-format
msgid "Show Diagnostics Highlights"
msgstr ""

#: lspclientpluginview.cpp:688
#, kde-format
msgid "Show Diagnostics Marks"
msgstr ""

#: lspclientpluginview.cpp:691
#, kde-format
msgid "Show Diagnostics on Hover"
msgstr ""

#: lspclientpluginview.cpp:694
#, kde-format
msgid "Switch to Diagnostics Tab"
msgstr ""

#: lspclientpluginview.cpp:698
#, kde-format
msgid "Show Messages"
msgstr ""

#: lspclientpluginview.cpp:703
#, kde-format
msgid "Server Memory Usage"
msgstr ""

#: lspclientpluginview.cpp:707
#, kde-format
msgid "Close All Dynamic Reference Tabs"
msgstr ""

#: lspclientpluginview.cpp:709
#, kde-format
msgid "Restart LSP Server"
msgstr ""

#: lspclientpluginview.cpp:711
#, kde-format
msgid "Restart All LSP Servers"
msgstr ""

#: lspclientpluginview.cpp:722
#, kde-format
msgid "Go To"
msgstr ""

#: lspclientpluginview.cpp:750
#, kde-format
msgid "More options"
msgstr ""

#: lspclientpluginview.cpp:782
#, kde-format
msgid "LSP"
msgstr ""

#: lspclientpluginview.cpp:978 lspclientpluginview.cpp:2527
#: lspclientsymbolview.cpp:285
#, kde-format
msgid "Expand All"
msgstr ""

#: lspclientpluginview.cpp:979 lspclientpluginview.cpp:2528
#: lspclientsymbolview.cpp:286
#, kde-format
msgid "Collapse All"
msgstr ""

#: lspclientpluginview.cpp:1001
#, kde-format
msgctxt "@title:tab"
msgid "Diagnostics"
msgstr ""

#: lspclientpluginview.cpp:1253
#, kde-format
msgid "RangeHighLight"
msgstr ""

#: lspclientpluginview.cpp:1259
#, kde-format
msgid "Error"
msgstr ""

#: lspclientpluginview.cpp:1263
#, kde-format
msgid "Warning"
msgstr ""

#: lspclientpluginview.cpp:1267
#, kde-format
msgid "Information"
msgstr ""

#: lspclientpluginview.cpp:1451
#, kde-format
msgctxt "@info"
msgid ""
"Error in regular expression: %1\n"
"offset %2: %3"
msgstr ""

#: lspclientpluginview.cpp:1778
#, kde-format
msgid "Line: %1: "
msgstr ""

#: lspclientpluginview.cpp:1925 lspclientpluginview.cpp:2277
#: lspclientpluginview.cpp:2398
#, kde-format
msgid "No results"
msgstr ""

#: lspclientpluginview.cpp:1984
#, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr ""

#: lspclientpluginview.cpp:1990
#, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr ""

#: lspclientpluginview.cpp:1996
#, kde-format
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr ""

#: lspclientpluginview.cpp:2002
#, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr ""

#: lspclientpluginview.cpp:2014
#, kde-format
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr ""

#: lspclientpluginview.cpp:2027
#, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr ""

#: lspclientpluginview.cpp:2051 lspclientpluginview.cpp:2063
#: lspclientpluginview.cpp:2076
#, kde-format
msgid "No Actions"
msgstr ""

#: lspclientpluginview.cpp:2067
#, kde-format
msgid "Loading..."
msgstr ""

#: lspclientpluginview.cpp:2159
#, kde-format
msgid "No edits"
msgstr ""

#: lspclientpluginview.cpp:2235
#, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr ""

#: lspclientpluginview.cpp:2236
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr ""

#: lspclientpluginview.cpp:2283
#, kde-format
msgid "Not enough results"
msgstr ""

#: lspclientpluginview.cpp:2353
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr ""

#: lspclientpluginview.cpp:2537
#, kde-format
msgid "Copy to Clipboard"
msgstr ""

#: lspclientpluginview.cpp:2560
#, kde-format
msgid "Remove Global Suppression"
msgstr ""

#: lspclientpluginview.cpp:2562
#, kde-format
msgid "Add Global Suppression"
msgstr ""

#: lspclientpluginview.cpp:2566
#, kde-format
msgid "Remove Local Suppression"
msgstr ""

#: lspclientpluginview.cpp:2568
#, kde-format
msgid "Add Local Suppression"
msgstr ""

#: lspclientpluginview.cpp:2580
#, kde-format
msgid "Disable Suppression"
msgstr ""

#: lspclientpluginview.cpp:2582
#, kde-format
msgid "Enable Suppression"
msgstr ""

#: lspclientpluginview.cpp:2728
#, kde-format
msgctxt "@info"
msgid "%1 [suppressed: %2]"
msgstr ""

#: lspclientpluginview.cpp:2843 lspclientpluginview.cpp:2880
#, kde-format
msgctxt "@info"
msgid "LSP Server"
msgstr ""

#: lspclientpluginview.cpp:2903
#, kde-format
msgctxt "@info"
msgid "LSP Client"
msgstr ""

#: lspclientservermanager.cpp:576
#, kde-format
msgid "Restarting"
msgstr ""

#: lspclientservermanager.cpp:576
#, kde-format
msgid "NOT Restarting"
msgstr ""

#: lspclientservermanager.cpp:577
#, kde-format
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr ""

#: lspclientservermanager.cpp:773
#, kde-format
msgid "Failed to find server binary: %1"
msgstr ""

#: lspclientservermanager.cpp:776 lspclientservermanager.cpp:808
#, kde-format
msgid "Please check your PATH for the binary"
msgstr ""

#: lspclientservermanager.cpp:777 lspclientservermanager.cpp:809
#, kde-format
msgid "See also %1 for installation or details"
msgstr ""

#: lspclientservermanager.cpp:805
#, kde-format
msgid "Failed to start server: %1"
msgstr ""

#: lspclientservermanager.cpp:813
#, kde-format
msgid "Started server %2: %1"
msgstr ""

#: lspclientservermanager.cpp:847
#, kde-format
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr ""

#: lspclientservermanager.cpp:850
#, kde-format
msgid "Failed to parse server configuration '%1': %2"
msgstr ""

#: lspclientservermanager.cpp:854
#, kde-format
msgid "Failed to read server configuration: %1"
msgstr ""

#: lspclientsymbolview.cpp:238
#, kde-format
msgid "Symbol Outline"
msgstr ""

#: lspclientsymbolview.cpp:276
#, kde-format
msgid "Tree Mode"
msgstr ""

#: lspclientsymbolview.cpp:278
#, kde-format
msgid "Automatically Expand Tree"
msgstr ""

#: lspclientsymbolview.cpp:280
#, kde-format
msgid "Sort Alphabetically"
msgstr ""

#: lspclientsymbolview.cpp:282
#, kde-format
msgid "Show Details"
msgstr ""

#: lspclientsymbolview.cpp:437
#, kde-format
msgid "Symbols"
msgstr ""

#: lspclientsymbolview.cpp:568
#, kde-format
msgid "No LSP server for this document."
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, kde-format
msgid "Client Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:47
#, kde-format
msgid "Completions:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:54
#, kde-format
msgid "Show inline docs for selected completion"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:61
#, kde-format
msgid "Show function signature when typing a function call"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: lspconfigwidget.ui:75
#, kde-format
msgid "Diagnostics:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:82
#, kde-format
msgid "Show program diagnostics"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnosticsHighlight)
#: lspconfigwidget.ui:97
#, kde-format
msgid "Highlight lines with diagnostics"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnosticsMark)
#: lspconfigwidget.ui:104
#, kde-format
msgid "Show markers in the margins for lines with diagnostics"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnosticsHover)
#: lspconfigwidget.ui:111
#, kde-format
msgid "Show diagnostics on hover"
msgstr ""

#. i18n: ectx: property (toolTip), widget (QSpinBox, spinDiagnosticsSize)
#: lspconfigwidget.ui:118
#, kde-format
msgid "max diagnostics tooltip size"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:133
#, kde-format
msgid "Navigation:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:140
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:147
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:154
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:161
#, kde-format
msgid "Server:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:168
#, kde-format
msgid "Show notifications from the LSP server"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:175
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:182
#, kde-format
msgid "Display additional details for symbols"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:189
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:204
#, kde-format
msgid "Automatically expand tree"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:213
#, kde-format
msgid "Sort symbols alphabetically"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:220
#, kde-format
msgid "Document outline:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Enable semantic highlighting"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:241
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:248
#, kde-format
msgid "Format on save"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:271
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:281
#, kde-format
msgid "User Server Settings"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:289
#, kde-format
msgid "Settings File:"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:316
#, kde-format
msgid "Default Server Settings"
msgstr ""

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, kde-format
msgid "More Options"
msgstr ""
